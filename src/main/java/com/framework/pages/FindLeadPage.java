package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods {

	public FindLeadPage() {

		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.NAME, using = "firstName") WebElement eleFirstName;
	@FindBy(how = How.XPATH,using = "//button[contains (text() , 'Find Leads')]")WebElement eleFindLead;
	@FindBy(how = How.XPATH, using = "(//a[@class = 'linktext'])[6]")WebElement eleResFname;

	public FindLeadPage EnterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;

	}

	public FindLeadPage ClickFindLead() {
		click(eleFindLead);
		return this;
	}
	public ViewLeadPage ClickResFirstName() {
		click(eleResFname);
		return new ViewLeadPage();
	}




}

