package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadsPage extends ProjectMethods{

	public CreateLeadsPage(){

		PageFactory.initElements(driver, this);

	}
	@FindBy(how = How.ID,using="createLeadForm_firstName")WebElement eleFirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName")WebElement eleLastName;
	@FindBy(how = How.ID,using="createLeadForm_companyName")WebElement eleCompanyName;
	@FindBy(how = How.NAME,using="submitButton")WebElement eleSubmit;

	public CreateLeadsPage EnterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public CreateLeadsPage EnterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	
	public CreateLeadsPage EnterCompanyName (String data) {
		
		clearAndType(eleCompanyName,data);
		return this;
	}
	
	public ViewLeadPage ClickSubmit() {
		click(eleSubmit);
		return new ViewLeadPage();
	}
	
	
	
	
}