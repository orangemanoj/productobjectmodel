package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadsPage;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyLeadsPage;
import com.framework.pages.ViewLeadPage;

public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "Create Lead in leaftaps";
		testNodes = "Leads";
		author = "Manoj";
		category = "smoke";
		dataSheetName = "TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String username, String password,String FirstName, String LastName, String CompanyName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin();
		
		new HomePage()
		.clickCRMSF();		
		
		new MyHomePage()
		.ClickLeads();
		
		new MyLeadsPage()
		.clickCreateLeads();
		
		new CreateLeadsPage()
		.EnterFirstName(FirstName)
		.EnterLastName(LastName)
		.EnterCompanyName(CompanyName)
		.ClickSubmit();
		
		new ViewLeadPage()
		.VerifyFname(FirstName);
	}
	
	

}
