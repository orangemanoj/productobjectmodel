package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.FindLeadPage;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyLeadsPage;
import com.framework.pages.ViewLeadPage;

public class TC003_FindLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_FindLead";
		testDescription = "Find Lead in leaftaps";
		testNodes = "Leads";
		author = "Manoj";
		category = "smoke";
		dataSheetName = "TC002";
	}

	@Test(dataProvider = "fetchData")
	public void FindLead(String username, String password,String FirstName, String LastName, String CompanyName) throws InterruptedException {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin();
		
		new HomePage()
		.clickCRMSF();		
		
		new MyHomePage()
		.ClickLeads();
		
		new MyLeadsPage()
		.ClickFindLead();
		
		Thread.sleep(3000);
		
		new FindLeadPage()
		.EnterFirstName(FirstName)
		.ClickFindLead()
		.ClickResFirstName();
		
		new ViewLeadPage()
		.VerifyFname(FirstName);
		
		
		
	}
	
}
